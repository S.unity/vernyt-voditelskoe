<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'sunity' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1JLFp<)}HX(K4Gl26t9tFpd{vKE>-Q4V7!PS):J_1R:@ET7mMV.Lqq>Y0)nqPF9t' );
define( 'SECURE_AUTH_KEY',  'w9^MkT~.lM`PVD8}3md < l6Vo:{fc%raCR#D8%0dBej3=,hc?7$A2.V_BadDym0' );
define( 'LOGGED_IN_KEY',    '.SBKzXB//;tduE8rFC6xot//gurY2WH_ihBSbo+ 6cl/h[0R8f}&ezNk+1CJHdOD' );
define( 'NONCE_KEY',        '/U6[H}_10 _mUN~Y?(B*bgh-y|0Il=L@vs[%TmS;EL`p|0cc%&.p,!hH<a=x4_%.' );
define( 'AUTH_SALT',        '6N)ex:1=VcP~`OqG7pY^ID4gZ[,oHDlC9nLTV/LWaG<&> 3FIUQdu&v+dFrFg=S*' );
define( 'SECURE_AUTH_SALT', 'KIfM rlB($x#CZUxZ@co-GPdbDI~BsvP..6;6a,guHyS-#>zU0]-K rTZB>!AF-A' );
define( 'LOGGED_IN_SALT',   'BFbKC;:zXpdz3H6LAt6%L;`]`_rCc<RrJHnZpom&+Kx1VB6k5Ldx6*PVwd+#O+|H' );
define( 'NONCE_SALT',       'K(,Zoy!D,KdJkF0PHs;grboICS=rl?C[D1I-]GwBH0Eq5)JK+Pypsaw?P&ivc-C6' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';

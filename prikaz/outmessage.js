let stageCase = document.getElementsByName('form[95572]');
let caseMaterials = document.getElementsByName('form[74258]');
let howMuchTime = document.getElementsByName('form[46953]');
let outcome
function sendCRMBez(){
    // ниже циклы, которые вычисляют какой элемент из списка выбран
    let cause = document.getElementsByName('form[81531]')[0].value;
    for (var i=0;i<stageCase.length; i++) {
        if (stageCase[i].checked) {
            stageCase = stageCase[i].value;
        }
    };
    for (var i=0;i<caseMaterials.length; i++) {
        if (caseMaterials[i].checked) {
            caseMaterials = caseMaterials[i].value;
        }
    };
    for (var i=0;i<howMuchTime.length; i++) {
        if (howMuchTime[i].checked) {
            howMuchTime = howMuchTime[i].value;
        }
    };
    outcome = '● С чем связано дело? : ' + stageCase + '<br>' +
        '● На какой стадии дело? : ' + caseMaterials + '<br>' +
        '● Сумма взыскания : ' + cause + '<br>';
};
// нажатие кнопки "отправить"
$('#btnSend').on('click', function(){
    console.log('click')
    let city = document.getElementsByName('form[31998]')[0].value;
    let detailCase = document.getElementsByName('form[13169]')[0].value;
    let email = document.getElementsByName('form[13168]')[0].value;
    let phone = document.getElementsByName('form[56070]')[0].value;
    sendCRMBez();
    let formData = new FormData();
    formData.append('crm', '16');
    formData.append('pipe', '34');
    formData.append('contact[467]', email)
    formData.append('contact[466]', phone);
    formData.append('contact[473]', city);
    formData.append('lead[471]', city);
    formData.append('lead[541]', 'приказ.мфюц.рф');
    formData.append('note', 'сайт: приказ.мфюц.рф <br>' + outcome + '● Обстоятельства дела: ' + detailCase);
    axios.post('https://bez.v-avtoservice.com/api/import-lead',
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    ).then(function (response) {
        $('.close').trigger('click');
        Swal.fire({
            title: '<strong>Спасибо!</strong>',
            icon: 'success',
            html:
                'Мы отменим судебный приказ с гарантией. Юрист свяжется с Вами.<br><br>Выберите удобный способ для связи:<div style="margin-top: 30px;"><a href="https://wa.me/79250194968" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/wa.jpg" alt="WhatsApp" style="width: 120px;"></a><a href="tg://resolve?domain=Avtourist_bot" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/tg.jpg" alt="Telegram" style="width: 120px;"></a><a href="viber://pa?chatURI=avtourist-bot&context=welcome" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vb.jpg" alt="Viber" style="width: 120px;"></a><a href="https://vk.com/write-7375007" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vk.jpg" alt="VK" style="width: 120px;"></a></div></div>',
            showCloseButton: true,
            focusConfirm: false,
            showConfirmButton: false
        })
    });
});

// кнопка в футере
let getConsultationBtn = document.getElementById('getConsultationBtn')
let footerContainer = document.getElementById('footerContainer')
let footerPopupClosed = document.getElementById('footerPopupClosed')
let submitFooterForm = document.getElementById('submitFooterForm')
document.addEventListener("click", (e) => {
    if (getConsultationBtn === e.target) {
        getConsultationBtn.classList.add('click')
        footerContainer.classList.add('click')
    } else if ( footerPopupClosed === e.target) {
        getConsultationBtn.classList.remove('click')
        footerContainer.classList.remove('click')
    } else if ( submitFooterForm === e.target) {
        submitFooterForm.innerText = "Ждите.."
        let footerPopupName = document.getElementsByName('footerPopupName')[0].value
        let footerPopupEmail = document.getElementsByName('footerPopupEmail')[0].value
        let footerPopupPhone = document.getElementsByName('footerPopupPhone')[0].value
        let formData = new FormData();
        formData.append('crm', '11');
        formData.append('pipe', '22');
        formData.append('contact[name] ', footerPopupName);
        formData.append('contact[199]', footerPopupPhone);
        formData.append('contact[200]', footerPopupEmail);
        formData.append('lead[214]', 'Вернуть-водительское.рф');
        formData.append('note', 'сайт: Вернуть-водительское.рф <br> Хотят связаться с специалистом');
        axios.post('https://urist.v-avtoservice.com/api/import-lead',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then(function (response) {
            getConsultationBtn.classList.remove('click')
            footerContainer.classList.remove('click')
            submitFooterForm.innerText = "Отправить"
            Swal.fire({
                title: '<strong>Спасибо!</strong>',
                icon: 'success',
                html:
                    'Мы свяжемся с Вами и ответим на все Ваши вопросы.<br><br>Выберите удобный способ для связи:<div style="margin-top: 30px;"><a href="https://wa.me/79250194968" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/wa.jpg" alt="WhatsApp" style="width: 120px;"></a><a href="tg://resolve?domain=Avtourist_bot" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/tg.jpg" alt="Telegram" style="width: 120px;"></a><a href="viber://pa?chatURI=avtourist-bot&context=welcome" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vb.jpg" alt="Viber" style="width: 120px;"></a><a href="https://vk.com/write-7375007" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vk.jpg" alt="VK" style="width: 120px;"></a></div></div>',
                showCloseButton: true,
                focusConfirm: false,
                showConfirmButton: false
            })
        });
    }
})
// закрытие вкладки
// function closeTabs () {
//     let createModal = document.getElementById('modal')
//     $(document).mouseleave(function (e) {
//         if (e.clientY < 10000000000000) {
//             createModal.style.display = "block";
//             document.addEventListener('click', (e) => {
//                 if (e.target === createModal) {
//                     createModal.remove()
//                 }
//             })
//         }
//         return null
//     })
// }
// closeTabs();
$('#closeTabs').click(function () {
    let closeTabs = document.getElementById('closeTabs')
    closeTabs.innerText = 'Подождите..'
    let name, phone, email;
    name = document.getElementById('name').value;
    phone = document.getElementById('phone').value; // тут обозначаем переменные, чтобы вытащить из них значения и отправить методом axios
    email = document.getElementById('email').value;
    let formData = new FormData();
    formData.append('crm', '11');
    formData.append('pipe', '22');
    formData.append('contact[name]', name);
    formData.append('contact[199]', phone);
    formData.append('contact[200]', email);
    formData.append('lead[214]', 'Вернуть-водительское.рф');
    formData.append('note', 'сайт: Вернуть-водительское.рф <br>' + 'пользователь хотел закрыть страницу');
    axios.post('https://urist.v-avtoservice.com/api/import-lead',
        formData,
        {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
    ).then(function (response) {
        document.getElementById('modal').remove()
        Swal.fire({
            title: '<strong>Спасибо!</strong>',
            icon: 'success',
            html:
                'Мы свяжемся с Вами и ответим на все Ваши вопросы.<br><br>Выберите удобный способ для связи:<div style="margin-top: 30px;"><a href="https://wa.me/79250194968" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/wa.jpg" alt="WhatsApp" style="width: 120px;"></a><a href="tg://resolve?domain=Avtourist_bot" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/tg.jpg" alt="Telegram" style="width: 120px;"></a><a href="viber://pa?chatURI=avtourist-bot&context=welcome" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vb.jpg" alt="Viber" style="width: 120px;"></a><a href="https://vk.com/write-7375007" style="border-bottom:none;display: inline-block;margin:5px 3px;"><img src="img/soc/vk.jpg" alt="VK" style="width: 120px;"></a></div></div>',
            showCloseButton: true,
            focusConfirm: false,
            showConfirmButton: false
        })
    });
    return false;
})

let btnWinsCase = document.getElementById('btnWinsCase')
let winsCasePage = document.getElementById('winsCasePage')
let closeWinsCasePage = document.getElementById('closeWinsCasePage')
document.addEventListener('click', (e) => {
    if (btnWinsCase === e.target) {
        winsCasePage.style.display = 'block'
        document.body.style.overflowY = 'hidden'
    } else if (closeWinsCasePage === e.target) {
        winsCasePage.style.display = 'none'
        document.body.style.overflowY = 'scroll'
    }
})
